# Vessel Classification

## Overview

this work had 4 parts
1. ETL
2. Training model
3. Evaluating Model
4. Prediction

### ETl

this module takes data and performs feature extraction.

to run the etl

```
python3 src/etl.py -h


usage: etl.py [-h] [--path PATH] [--ports_data PORTS_DATA]
              [--vessels_data VESSELS_DATA]

optional arguments:
  -h, --help            show this help message and exit
  --path PATH, -p PATH  path to save data
  --ports_data PORTS_DATA
                        path to port data
  --vessels_data VESSELS_DATA
                        path to vessel data
```

### Training Model

this model trains and evaluates model

to rum model training

```
python3 src/train_model.py -h

usage: train_model.py [-h] [--path PATH]

optional arguments:
  -h, --help            show this help message and exit
  --path PATH, -p PATH  path to data file
```

### Evaluating model

src/train_model.py does the model evaluation

the model used in the prediction had scored:

```
{
    'score': 0.9040893818122218,
    'roc_auc': 0.906092892577306,
    'accuracy': 0.9040893818122218,
    'precision': 0.482272890056106,
    'sensitivity': 0.6007434944237918,
    'specificity': 0.9347711651551385
}
```

generally the evaluation approach is to balance precision vs. sensitivity, in order to minimize false-negativity and false-positivity.

to increase robustness data is random-shuffled twice, when sampling and when splitting, so the model evaluates itself on different data each time

metrics used: roc_auc, confusion_matrix

refer to:[link](https://medium.com/greyatom/performance-metrics-for-classification-problems-in-machine-learning-part-i-b085d432082b)

### Prediction

to run
```
python3 src/test.py
```


prediction was done in two phases. in the port test data there are ±200k rows but only ±5200 unique vessel ids
hence, prediction was done on all data, then data was grouped by id and aggregated by most frequent value

