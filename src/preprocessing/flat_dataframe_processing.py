from os.path import isfile
import pandas as pd


class HandleNaStrategy():
    DROP = 'drop'
    FILL_MOST_FREQUENT = 'freq'


def handle_nan(df, target='drop'):
    if target == HandleNaStrategy.DROP:
        return df.dropna()
    if target == HandleNaStrategy.FILL_MOST_FREQUENT:
        return df.apply(
            lambda row: row.fillna(row.value_counts().index[0])
        )
    

def validate_data_uniqueness(main_df, other, col):
    """
    returns the difference between two dataframes
    unique values
    """
    diff = other[col].nunique() - main_df[col].nunique()
    if diff <= 0:
        return True
    else:
        return False


def join_data_by_id(
    df_port: pd.core.frame.DataFrame,
    df_ves: pd.core.frame.DataFrame,
    id_col_port: str,
    id_col_ves: str,
):
    """
    joins data between vessel data and port data
    args:
        df_port(Dataframe): port data ,
        df_ves(Dataframe): vessel data,
        id_col_port(str): vessel id in port data column name,
        id_col_ves(str): vessel id in vessel data column name,
    return:
        join_df(Dataframe): joined data frame, Indexed by id
    """
    df_ves = df_ves.rename(
        {
            id_col_ves: id_col_port
        },
        axis='columns'
    )
    assert(validate_data_uniqueness(df_ves, df_port, id_col_port))
    return pd.merge(
        df_port,
        df_ves,
        on=[id_col_port],
    )


def read_data(path: str):
    """
    reads data from parquet / csv file to pandas dataframe
    if file is not parquet / csv or not file will raise ValueError
    args:
        path(str): path to file
    returns:
        df(Dataframe): pandas dataframe
    """
    if isfile(path):
        if path.endswith(".csv"):
            return pd.read_csv(
                path,
                engine="python"
            )
        elif path.endswith(".parquet"):
            return pd.read_parquet(
                path
            )
        else:
            raise ValueError(
                "file must be of type str / parquet" +
                " while file is of type {}".format(
                    path.split('.')[-1]
                )
            )
    else:
        raise ValueError(
            "file {} must be a file".format(
                path
            )
        )


def shuffle_dataframe(df):
    from time import time
    return df.sample(
        frac=1,
        random_state=int(time())
    ).reset_index(drop=True)

