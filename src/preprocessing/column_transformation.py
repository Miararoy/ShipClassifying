import re
import pycountry
import datetime
from pytz import country_timezones, utc
import pandas as pd


def get_tz_by_country(country: str):
        return country_timezones(
            pycountry.countries.get(name=country)
        ).alpha_2.pop()


def get_tz_mapping(
    df,
    country_col: str
):
    def map_irr(country):
        irr = {
            "South Korea": "Korea, Republic of",
            "Taiwan": "China",
            "Russia": "Russian Federation",
            "Vietnam": "Viet Nam",
            "Venezuela": "Venezuela, Bolivarian Republic of",
            "Iran": "Iran, Islamic Republic of",
            "Trinidad And Tobago": "Trinidad and Tobago",
            "Ivory Coast": "Côte d'Ivoire",
            "Tanzania": "Tanzania, United Republic of",
            "Saint Vincent And The Grenadines": "Saint Vincent and the Grenadines",
            "Cape Verde": "Cabo Verde",
            "Malvinas": "Falkland Islands (Malvinas)",
            "Republic of the Congo": "Congo, The Democratic Republic of the",
            "Democratic Republic of the Congo": "Congo, The Democratic Republic of the",
            "None": "United Kingdom",
            "Saint Kitts And Nevis": "Saint Kitts and Nevis",
            "Antigua And Barbuda": "Antigua and Barbuda",
            "Micronesia": "Micronesia, Federated States of",
            "Syria": "Syrian Arab Republic",
            "Brunei": "Brunei Darussalam",
            "Sao Tome And Principe": "Sao Tome and Principe",
            "Guinea Bissau": "Guinea-Bissau",
            "East Timor": "Timor-Leste",
            "South Georgia and South Sandwich": "South Georgia and the South Sandwich Islands",
            "North Korea": "Korea, Democratic People's Republic of",
        }
        irr_fixed_name = irr.get(country.replace("  ", " "), country.replace("  ", " "))
        return irr_fixed_name

    def get_tz_by_country(country: str):
        try:
            country_alpha_2 = pycountry.countries.get(
                name=map_irr(" ".join(re.findall('[A-Z][^A-Z]*', country)))
            ).alpha_2
            tz = country_timezones(country_alpha_2).pop() if country_timezones(country_alpha_2) else 'UTC'
        except AttributeError:
            tz = utc
        return tz

    country_to_tz = {
        k: get_tz_by_country(k) for k in list(
            df[country_col].value_counts().keys()
        )
    }
    return country_to_tz


def make_ts_column(
    df,
    ts_column
):
    df[ts_column] = pd.to_datetime(df[ts_column], errors='coerce')
    return df


def concat_column_values(
    df,
    columns: list
):
    df["_".join(columns)] = df[columns].apply(
        lambda r: "_".join(r),
        axis=1
    )
    return df.drop(columns, axis=1)


def convert_utc_ts_to_local(
    df,
    ts_column: str,
    country_column: str
):
    country_to_tz_mapping = get_tz_mapping(df, country_column)

    def convert_tz(row):
        new_ts = row[ts_column].replace(tzinfo=datetime.timezone.utc).astimezone(
            tz=country_to_tz_mapping.get(row[country_column])
        ).replace(tzinfo=None)
        return new_ts
 
    df["local_" + ts_column] = df.apply(
        lambda r: convert_tz(r),
        axis=1
    )
    return df


def beakdown_timestamp(
    df,
    ts_column: str
):
    df[ts_column + "_DAY_OF_WEEK"] = df[ts_column].dt.weekday_name
    df[ts_column + "_HOUR_OF_DAY"] = df[ts_column].dt.hour
    if "local" not in ts_column: 
        df[ts_column + "_MONTH"] = df[ts_column].dt.month
        df[ts_column + "_YEAR"] = df[ts_column].dt.year

    return df.drop(ts_column, axis=1)
