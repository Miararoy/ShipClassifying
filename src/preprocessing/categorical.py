from collections import namedtuple

PreparedData = namedtuple("prepared_data", ["X", "Y", "cat_mapping"])


def convert_data_to_categorical(
        df,
        list_of_categorical_columns: list,
        categories: dict = None
):
    """
    converts all columns in df to categorical
    Args:
        df (DataFrame): pandas dataframe
        list_of_categorical_columns (list): list of all categorical columns
    Returns:
        cat_df (DataFrame): pandas dataframes where columns are categorical
        categorical_mapping (dict): dict(column -> dict(index -> cate_value) 
            this should assist reverse-engineering lightGBM
    """
    categorical_mapping = categories or {}
    for column in list_of_categorical_columns:
        if categories is not None:
            df[column] = df[column].astype(
                'category',
                categories=categories[column]
            )
        else:
            df[column] = df[column].astype('category')
            categorical_mapping[column] = list(
                df[column].cat.categories
            )
    return df, categorical_mapping


def digest_data(
        df,
        columns: list,
        label: str = None,
        index_column: str = None,
        categorical_columns: list = None,
        categories_mapping: dict = None,
):
    """
    this method prepares data frame for training on lighGBM
    Args:
        file_path: str - path to file
        y_column: str - name of the result column
        columns_to_drop: list - which columns should be dropped if any
        index_column: str - name of id column
        non_categorical: list - list of all non categorical columns
        categories: dict - if already has categorical mapping of the data
        n_sample: int - size of sample, if None - will take all data
        
    Returns:
        df (DataFrame): data frame with all columns categorical
    """
    df = df[columns]
    #
    # Handle categorical data
    #
    df, cat_mapping = convert_data_to_categorical(
            df,
            categorical_columns,
            categories_mapping
        )
    #
    # Set index column
    #
    if index_column is not None:
        df = df.set_index(index_column)
    #
    # Set x,y dataframes
    #
    if label is not None:
        if isinstance(label, str):
            y = df[label]
            df = df.drop(label, axis=1)
            x = df
    else:
        x = df
    #
    # debug print
    #
    print("training set of shape: {shape} with features: {cols}".format(
        shape=x.shape,
        cols=list(x.columns)
    ))
    if label is not None:
        print("result set {cols} of shape: {shape}".format(
            shape=y.shape,
            cols=y.name
        ))
    if label is not None:
        return PreparedData(x, y, cat_mapping)
    else:
        return PreparedData(x, None, cat_mapping)
