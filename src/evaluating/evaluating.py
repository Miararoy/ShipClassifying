def convert_prediction_results_to_binary(results: list):
    binary_results = []
    for x in results:
        if x >= 0.5:
            binary_results.append(1)
        else:
            binary_results.append(0)
    return binary_results


def calc_score(y1: list, y2: list):
    """
    calculated the % of similarity between 2 lists of binary elements
    Args:
        y1(list): binary set 1
        y2(list): binary set 2
    Returns:
        score(float): percenage of success
    """
    sum_similar = 0
    for i, j in zip(y1, y2):
        if i == j:
            sum_similar += 1
    score = sum_similar / max(len(y1), len(y2))
    return score
