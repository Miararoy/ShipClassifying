from collections import namedtuple

ConfusionMatrix = namedtuple(
    "ConfusionMatrix",
    ["true_negative", "false_positive", "false_negative", "true_positive"]
)


class Score(object):
    """
    Classifier score object
    """
    def __init__(
        self,
        raw_score,
        roc_auc,
        confusion_matrix
    ):
        """
        initilazies score object with metrics:
        args:
            raw_score(float): raw prediction results,
            roc_auc(float): area under roc curve ,
            confusion_matrix(tuple): confusion matrix
        """
        self.raw_score = raw_score
        self.roc_auc = roc_auc
        self.confusion_matrix = ConfusionMatrix(
            confusion_matrix[0],
            confusion_matrix[1],
            confusion_matrix[2],
            confusion_matrix[3],
        )
        self.total_test_samples = sum(confusion_matrix)
        self.precision = self.confusion_matrix.true_positive / (self.confusion_matrix.true_positive + self.confusion_matrix.false_positive)
        self.accuracy = (self.confusion_matrix.true_negative + self.confusion_matrix.true_positive) / self.total_test_samples
        self.Sensitivity = self.confusion_matrix.true_positive / (self.confusion_matrix.true_positive + self.confusion_matrix.false_negative)
        self.Specificity = self.confusion_matrix.true_negative / (self.confusion_matrix.true_negative + self.confusion_matrix.false_positive)
        self.W = [0.2, 0.4, 0.4]
    
    def set_weights(self, weights: list):
        if len(weights) == 3 and sum(weights) == 1.0:
            self.W = weights
        else:
            raise ValueError(
                "weights must be of len 3 and sum 1.0"
            )
    
    def get_score_details(self):
        score = {
            "score": self.raw_score,
            "roc_auc": self.roc_auc,
            "accuracy": self.accuracy,
            "precision": self.precision,
            "sensitivity": self.Sensitivity,
            "specificity": self.Specificity,
        }
        print(
            score
        )
        return score

    def score(self):
        # score = (
        #     self.W[0] * self.raw_score +
        #     self.W[1] * self.roc_auc +
        #     self.W[2] * (
                
        #     )
        pass
    
        