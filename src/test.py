import lightgbm
from preprocessing.categorical import digest_data
from preprocessing.flat_dataframe_processing import read_data
from evaluating.evaluating import convert_prediction_results_to_binary
import argparse

TEST_SET = "data/data_test.parquet"
COLUMNS = [
    "duration_min",
    "country",
    "Lat",
    "Long",
    "port_name_port_id",
    "start_time_DAY_OF_WEEK",
    "start_time_HOUR_OF_DAY",
    "start_time_MONTH",
    "start_time_YEAR",
    "local_start_time_DAY_OF_WEEK",
    "local_start_time_HOUR_OF_DAY",
]
CATEGORICAL_COLUMNS = [
    "port_name_port_id",
    "local_start_time_DAY_OF_WEEK",
    "start_time_DAY_OF_WEEK",
    "country"
]

parser = argparse.ArgumentParser()
parser.add_argument(
    "--model",
    "-m",
    help="path to model file"
)

def test(model):
    data = read_data(TEST_SET)
    data = data.set_index("ves_id")
    data = digest_data(
        data,
        columns=COLUMNS,
        categorical_columns=CATEGORICAL_COLUMNS
    )
    print(data.X.head(5).to_string())
    data.X["label"] = convert_prediction_results_to_binary(
        lightgbm.Booster(model_file=model).predict(
            data.X
        )
    )
    print(data.X["label"].head(40).to_string())
    data.X.to_parquet("data/result/result.parquet")


if __name__ == '__main__':
    args = parser.parse_args()
    test(args.model)
    