import lightgbm
import os
from time import time
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, auc, confusion_matrix, classification_report
from preprocessing.categorical import digest_data
from preprocessing.flat_dataframe_processing import read_data, shuffle_dataframe
from evaluating.evaluating import convert_prediction_results_to_binary, calc_score
from evaluating.score import Score
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--path",
    "-p",
    help="path to data file",
    default="/tmp/data"
)


CATEGORICAL_COLUMNS = [
    "port_name_port_id",
    "local_start_time_DAY_OF_WEEK",
    "start_time_DAY_OF_WEEK",
    "country"
]

COLUMNS = [
    "duration_min",
    "country",
    "Lat",
    "Long",
    "port_name_port_id",
    "start_time_DAY_OF_WEEK",
    "start_time_HOUR_OF_DAY",
    "start_time_MONTH",
    "start_time_YEAR",
    "local_start_time_DAY_OF_WEEK",
    "local_start_time_HOUR_OF_DAY",
    "label"
]
LABEL_COLUMN = "label"

params = {
    'boosting_type': 'gbdt',
    'objective': 'binary',
    'metric': 'binary_logloss',
    "num_leaves": 2 ** 4,
    'scale_pos_weight': 4.0,
    "num_threads": 8,
    "tree_learner": "data",
    "feature_fraction": 0.8
}

LEARN_RATE_START = 0.1
EXP_RATE_CHANGE = 0.99
MIN_LEARN_RATE = 0.001
ITER_THRESHOLD = 10 ** 4

MODEL_FOLDER = "model/"
MODEL_NAME = str(int(time()))


def learnning_rate_func(iter):
    if iter < ITER_THRESHOLD:
        return LEARN_RATE_START
    elif LEARN_RATE_START * (EXP_RATE_CHANGE ** iter) >= MIN_LEARN_RATE:
        return LEARN_RATE_START * (
            EXP_RATE_CHANGE ** abs(iter - ITER_THRESHOLD)
        )
    else:
        return MIN_LEARN_RATE


def train_model(data_file):
    """
    training lightgbm classifier 
    """
    #
    # PREPARE DATA FOR TRAINING
    #
    digested_data = digest_data(
        shuffle_dataframe(read_data(data_file)),
        label=LABEL_COLUMN,
        columns=COLUMNS,
        categorical_columns=CATEGORICAL_COLUMNS,
        index_column=None
    )
    #
    # SPLIT TO TRAIN AND TEST SET
    #
    x, x_test, y, y_test = train_test_split(
        digested_data.X,
        digested_data.Y,
        random_state=int(time()),
        test_size=0.1
    )
    #
    # SPLIT TO TRAIN AND EVAL SET
    #
    x_train, x_eval, y_train, y_eval = train_test_split(
        x,
        y,
        random_state=int(time()),
        test_size=0.2
    )
    #
    # PREPARE LIGHTGBM DATASET OBJECTS
    #
    lgb_train = lightgbm.Dataset(
        x_train,
        y_train
    )
    lgb_eval = lightgbm.Dataset(
        x_eval,
        y_eval,
        reference=lgb_train
    )
    #
    # TRAIN MODEL
    #
    gbm = lightgbm.train(
        params,
        lgb_train,
        learning_rates=learnning_rate_func,
        num_boost_round=10**4,
        early_stopping_rounds=100,
        valid_sets=lgb_eval
    )

    path_to_candidate = os.path.join(
        MODEL_FOLDER,
        "CANDIDATE",
        MODEL_NAME
    )
    gbm.save_model(path_to_candidate)
    print("model candidate {}".format(path_to_candidate))
    #
    # TEST MODEL
    #
    print("Predicting... Testing sample size = {}".format(
        x_test.shape
    ))
    model = lightgbm.Booster(model_file=path_to_candidate)
    y_score = model.predict(
            x_test
        )
    y_pred = convert_prediction_results_to_binary(
        y_score
    )
    #
    #           ===== SCORING ======
    #
    fpr, tpr, _ = roc_curve(y_test, y_score)
    roc_auc = auc(fpr, tpr)
    score = Score(
        calc_score(
            y_pred,
            list(y_test)
        ),
        roc_auc,
        confusion_matrix(
            y_test,
            y_pred,
        ).ravel()
    ).get_score_details()
    #
    #           ===== PLOT ROC CURVE ======
    #
    plt.figure()
    lw = 2
    plt.plot(
        fpr, tpr, color='darkorange',
        lw=lw, label='ROC curve (area = %0.2f)' % roc_auc
    )
    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    plt.show()
    #
    #           ===== TRAIN ON THE LAST BIT ======
    #
    gbm = lightgbm.train(
        params,
        lightgbm.Dataset(x_test, y_test),
        init_model=path_to_candidate,
        learning_rates=lambda iter: 0.1,
        num_boost_round=100
    )


if __name__ == "__main__":
    args = parser.parse_args()
    train_model(
        args.path
    )