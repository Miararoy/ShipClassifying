import pandas as pd
from preprocessing.flat_dataframe_processing import read_data
 
to_label = pd.read_csv("data/vessels_to_label.csv", names=["ves_id"]).set_index("ves_id")
result = pd.read_parquet("data/result/result.parquet")
result = result.groupby("ves_id").agg(lambda x:x.value_counts().index[0])[["label"]]
print(to_label.index.value_counts())
to_label = to_label.merge(result, how='outer', left_index=True, right_index=True).fillna(0)
to_label["label"] = to_label.label.astype(int)
to_label.to_csv("submission/submission.csv")
print(to_label.to_string())
