import pandas as pd
from preprocessing.flat_dataframe_processing import join_data_by_id,\
                                                    read_data,\
                                                    handle_nan
from preprocessing.flat_dataframe_processing import HandleNaStrategy
from preprocessing.categorical import digest_data
from preprocessing.column_transformation import concat_column_values,\
                                                convert_utc_ts_to_local,\
                                                beakdown_timestamp,\
                                                make_ts_column,\
                                                get_tz_mapping
from sklearn.metrics import auc, roc_curve
from sklearn.model_selection import train_test_split
import argparse

parser = argparse.ArgumentParser()
parser.add_argument(
    "--path",
    "-p",
    help="path to save data",
    default="/tmp/data"
)
parser.add_argument(
    "--ports_data",
    help="path to port data",
    default="/tmp/data"
)
parser.add_argument(
    "--vessels_data",
    help="path to vessel data",
    default=None
)



def etl(train_file_port, train_file_vessel, path):
    train_port_data = handle_nan(
        read_data(train_file_port),
        HandleNaStrategy.DROP,
    )
    if train_file_vessel:
        train_vess_data = handle_nan(
            read_data(train_file_vessel),
            HandleNaStrategy.DROP,
        )

    train_port_data = concat_column_values(
        train_port_data,
        [
            "port_name",
            "port_id"
        ]
    )

    train_port_data = make_ts_column(
        train_port_data,
        "start_time"
    )

    train_port_data = convert_utc_ts_to_local(
        train_port_data,
        "start_time",
        "country"
    )

    train_port_data = beakdown_timestamp(
        train_port_data,
        "start_time"
    )

    train_port_data = beakdown_timestamp(
        train_port_data,
        "local_start_time"
    )
    if train_file_vessel:
        join_df = join_data_by_id(
            train_port_data,
            train_vess_data,
            "ves_id",
            "vessel_id",
        )
        join_df["type"] = join_df["type"].map(
            {
                "Bulk Carrier": 0,
                "Container Vessel": 1,
                "Tug": 6,
                "Oil Tanker": 3,
                "Fishing Vessel": 2,
                "Passenger Vessel": 4,
                "Reefer": 5,
            }
        )
    else:
        join_df = train_port_data

    join_df.to_parquet(path)


if __name__ == "__main__":
    args = parser.parse_args()
    etl(
        args.ports_data,
        args.vessels_data,
        args.path
    )